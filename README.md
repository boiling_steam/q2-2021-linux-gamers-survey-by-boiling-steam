# Boiling Steam Linux Gamers Survey Q2 2021

## Background

This repository contains partial answers from the Linux Gamers survey conducted by Boiling Steam (using Google Forms) in Q2 2021. Respondents were asked at the end of the survey for their consent to share or not their answers publicly, and based on that answer we excluded the answers from the ones who did not wish to share. Out of 1874 respondents, 1770 agreed to share their answers.

## LICENSE

This dataset is released under a [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license. Please do not forget attribution if you use it in any way.

## Loading the data

Please use semi-colons as data separators when importing the CSV documents in your favorite application. You should find data for 1770 respondents.

## Overall picture

There are two files in this repository:

- *survey_structure_document.csv* is a simple CSV file listing the overall structure of the questionaire, and more important the link between the question code and the actual question. The column "questions" is the actual question shown in the survey. The column "order" confirms the order seen by respondents. The column "part" is a simple category to describe the different parts of the survey. Finally, the last column "removed" mentions is a specific column was removed or not from the survey raw data. 

- *survey_data.csv* is the raw data in CSV format. The questions are coded here. You need to refer to the survey_structure_document to understand the meaning of the coding. For example, q2 is about the rough location of the respondent.

## Analysis

The following articles have already been published (using the full dataset):


- [Most Popular Gamepads for Linux Gamers](https://boilingsteam.com/the-most-popular-gamepads-with-linux-gamers-in-2021/)
- [VR on Linux - A Growing Market?](https://boilingsteam.com/vr-on-linux-a-growing-market/)
- [Which Distro for Linux Gaming?](https://boilingsteam.com/which-linux-distro-for-gaming-q2-2021-survey-results/)
- [AMD on the Brink of Taking Over?](https://boilingsteam.com/amd-on-the-brink-of-taking-over-survey-q2-2021/)
- [Cloud Gaming: Is it Going Anywhere?](https://boilingsteam.com/cloud-gaming-is-it-going-anywhere-survey-results/)
- [Where Do Linux Users Buy Games? Meet 5 Different Profiles](https://boilingsteam.com/where-do-linux-users-buy-games-meet-5-different-profiles/)
- [Linux Gaming Predictions for 2021: What did Survey Respondents Think?](https://boilingsteam.com/linux-gaming-predictions-for-2021-what-did-you-think-back-in-april/)
- [Popular Tools for Linux Gamers in 2021](https://boilingsteam.com/popular-tools-of-linux-gamers-in-2021/)

If you produce any new analysis from the shared data, please contact us so that we can link it here as well.
